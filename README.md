# Word Guess

## Description
A recreation of the hit game [Wordle by New York Times](https://www.nytimes.com/games/wordle/index.html). Made using [React](https://react.dev/) + Typescript + [Tailwind](https://tailwindcss.com/) + [Vite](https://vitejs.dev/). Play the game [here](https://eigelasinas.vercel.app/games/wordle).

## Features
 - 14,855 words
 - Fully responsive
 - On-screen keyboard
 - Animations

## Installation
```
<!-- install dependencies -->
npm install
<!-- run development server -->
npm run dev
<!-- build and preview -->
npm run build
npm run preview
```
## Screenshots
![word guess](images/screenshot.png)
