import { useCallback, useEffect, useMemo, useState } from "react"
import Guess from "./_ui/Guess"
import wordList from "./wordList"
import Keyboard from "./_ui/Keyboard"

type Status = "playing" | "win" | "lose"

const MAX_GUESSES = 6
const WORD_LENGTH = 5

export type LetterResult = "correct" | "present" | "absent"

function App() {
	const [solution] = useState(
		wordList[Math.floor(Math.random() * wordList.length)]!
	)
	const [status, setStatus] = useState<Status>("playing")
	const [guesses, setGuesses] = useState<string[]>([])
	const [currGuess, setCurrGuess] = useState("")
	const [correctLetters, setCorrectLetters] = useState(new Set<string>())
	const [presentLetters, setPresentLetters] = useState(new Set<string>())
	const [absentLetters, setAbsentLetters] = useState(new Set<string>())

	const [message, setMessage] = useState("")

	const resetGame = () => {
		wordList[Math.floor(Math.random() * wordList.length)]!
		setGuesses([])
		setCurrGuess("")
		setCorrectLetters(new Set<string>())
		setPresentLetters(new Set<string>())
		setAbsentLetters(new Set<string>())
		setStatus("playing")
		setMessage("")
	}

	const getResults = useCallback(
		(guess: string): LetterResult[] => {
			const guessArr = guess.split("")
			const solutionArr = solution.split("")
			const results: LetterResult[] = Array(WORD_LENGTH).fill("absent")
			const found: Record<string, number> = {}

			guessArr.forEach((char, i) => {
				if (solution[i] === char) {
					found[char] = (found[char] || 0) + 1
					results[i] = "correct"
				}
			})

			guessArr.forEach((char, i) => {
				if (results[i] === "correct") return
				if (
					solution.includes(char) &&
					solutionArr.filter((c) => c === char).length > (found[char] || 0)
				) {
					results[i] = "present"
					found[char] = (found[char] || 0) + 1
				} else {
					results[i] = "absent"
				}
			})

			return results
		},
		[solution]
	)

	const guessResults = useMemo(
		() => guesses.map(getResults),
		[guesses, getResults]
	)

	useEffect(() => {
		guesses.forEach((guess, gi) =>
			guess.split("").forEach((char, ci) => {
				if (guessResults[gi]?.[ci] === "correct") {
					setCorrectLetters((old) => new Set(old.add(char)))
				} else if (guessResults[gi]?.[ci] === "present") {
					setPresentLetters((old) => new Set(old.add(char)))
				} else if (guessResults[gi]?.[ci] === "absent") {
					setAbsentLetters((old) => new Set(old.add(char)))
				}
			})
		)
	}, [guessResults, guesses])

	const submitGuess = useCallback(() => {
		if (currGuess.length !== WORD_LENGTH) return
		if (guesses.includes(currGuess)) {
			setMessage("You already guessed that.")
			return
		}
		if (!wordList.includes(currGuess)) {
			setMessage("Sorry, that word isn't in our list.")
			return
		}
		setMessage("")
		setGuesses([...guesses, currGuess])
		setCurrGuess("")

		if (currGuess.toLowerCase() === solution.toLowerCase()) setStatus("win")
		else if (guesses.length + 1 === MAX_GUESSES) setStatus("lose")
	}, [currGuess, guesses, solution])

	const addChar = useCallback(
		(char: string) => {
			if (currGuess.length === WORD_LENGTH) return
			setCurrGuess((old) => old + char.toLowerCase())
		},
		[currGuess.length]
	)

	const eraseChar = () => {
		setCurrGuess((old) => old.slice(0, -1))
		setMessage("")
	}

	useEffect(() => {
		const handleKeyup = (e: KeyboardEvent) => {
			if (status !== "playing") return

			if (e.key.length === 1 && e.key.match(/[a-z]/i)) {
				addChar(e.key)
			} else if (e.key === "Backspace") {
				eraseChar()
			} else if (e.key === "Enter") {
				submitGuess()
			}
		}
		window.addEventListener("keyup", handleKeyup)
		return () => window.removeEventListener("keyup", handleKeyup)
	}, [addChar, status, submitGuess])

	return (
		<div className="h-full flex flex-col items-center justify-between py-5">
			<div className="flex flex-col items-center gap-2">
				{guesses.map((guess, i) => (
					<Guess
						key={i}
						guess={guess}
						wordLength={WORD_LENGTH}
						results={guessResults[i]}
					/>
				))}
				{status === "playing" && (
					<Guess guess={currGuess} wordLength={WORD_LENGTH} animateChange />
				)}
				{[
					...Array(
						Math.max(
							0,
							MAX_GUESSES - guesses.length - (status === "playing" ? 1 : 0)
						)
					),
				].map((_, i) => (
					<Guess key={i} guess={""} wordLength={WORD_LENGTH} />
				))}
			</div>
			<div className="text-xl my-3 min-h-[80px] flex flex-col justify-center">
				{status === "win" && <p>You Win!</p>}
				{status === "lose" && (
					<p>Game over. The correct answer was "{solution}".</p>
				)}
				{["win", "lose"].includes(status) && (
					<button
						className="bg-slate-500 px-4 py-1.5 block mx-auto mt-3 rounded-md"
						onClick={resetGame}
					>
						Try again
					</button>
				)}
				{status === "playing" && <p>{message}</p>}
			</div>
			<Keyboard
				correctLetters={Array.from(correctLetters)}
				presentLetters={Array.from(presentLetters)}
				absentLetters={Array.from(absentLetters)}
				onKeyClick={(key) => {
					if (key === "backspace") {
						eraseChar()
					} else if (key === "enter") {
						submitGuess()
					} else {
						addChar(key)
					}
				}}
			/>
		</div>
	)
}

export default App
