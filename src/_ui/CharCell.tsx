import { twJoin } from "tailwind-merge"
import { LetterResult } from "../App"

interface CharCellProps {
	char: string
	result?: LetterResult
	reveal?: boolean
	grow?: boolean
}

export default function CharCell({
	char,
	result,
	reveal = false,
	grow = false,
}: CharCellProps) {
	return (
		<div
			className={twJoin(
				"flipper h-14 w-14 ",
				reveal && "flip",
				grow && "scale-110"
			)}
		>
			<div
				className={twJoin(
					"flipper-inner  text-3xl uppercase font-bold",
					grow && "scale-110"
				)}
			>
				<div className="flex items-center justify-center flipper-front bg-neutral-900 border-2 border-neutral-600 rounded-sm">
					<h1>{char}</h1>
				</div>
				<div
					className={twJoin(
						"flex items-center justify-center flipper-back border-2 rounded-sm",
						{
							present: "bg-yellow-600 border-yellow-600",
							correct: "bg-green-600 border-green-600",
							absent: "bg-neutral-600 border-neutral-600",
							none: "",
						}[(reveal && result) || "none"]
					)}
				>
					{char}
				</div>
			</div>
		</div>
	)
}
