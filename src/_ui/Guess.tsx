import { LetterResult } from "../App"
import { useEffect, useState } from "react"
import CharCell from "./CharCell"

interface GuessProps {
	guess: string
	wordLength: number
	results?: LetterResult[]
	animateChange?: boolean
}

export default function Guess({
	guess,
	wordLength,
	results = [],
	animateChange = false,
}: GuessProps) {
	const [prevLength, setPrevLength] = useState(guess.length)
	const [shouldGrow, setShouldGrow] = useState(false)
	const [revealCount, setRevealCount] = useState(0)
	const [revealTimeoutID, setRevealTimeoutID] = useState(NaN)
	const shouldReveal = results.length != 0

	useEffect(() => {
		if (!animateChange) return
		setPrevLength(guess.length)
		if (guess.length <= prevLength) return

		setShouldGrow(true)

		window.setTimeout(() => {
			setShouldGrow(false)
		}, 100)
	}, [guess.length, animateChange, prevLength])

	useEffect(() => {
		if (revealCount === 0 || revealCount === wordLength) return
		console.log("reveal", revealCount)
		setRevealTimeoutID(
			window.setTimeout(() => setRevealCount(revealCount + 1), 300)
		)
	}, [revealCount, wordLength])

	useEffect(() => {
		if (!shouldReveal || revealCount !== 0) return
		console.log("set to 1")
		setRevealCount(1)
	}, [shouldReveal, revealCount, revealTimeoutID])

	return (
		<div className="flex gap-2">
			{[...Array(wordLength)].map((_, i) => {
				const char = guess[i] || ""

				return (
					<CharCell
						key={i}
						char={char}
						reveal={i <= revealCount - 1}
						result={results[i]}
						grow={animateChange && i === guess.length - 1 && shouldGrow}
					/>
				)
			})}
		</div>
	)
}
