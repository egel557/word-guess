import { twJoin } from "tailwind-merge"

interface KeyboardProps {
	onKeyClick: (key: string) => void
	correctLetters: string[]
	presentLetters: string[]
	absentLetters: string[]
}

export default function Keyboard({
	onKeyClick,
	correctLetters,
	presentLetters,
	absentLetters,
}: KeyboardProps) {
	const config = [
		["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
		["a", "s", "d", "f", "g", "h", "j", "k", "l"],
		["enter", "z", "x", "c", "v", "b", "n", "m", "backspace"],
	]

	return (
		<div className="flex flex-col gap-1 md:gap-2 items-center">
			{config.map((row, i) => (
				<div key={i} className="flex gap-1 md:gap-2">
					{row.map((key) => (
						<button
							key={key}
							className={twJoin(
								"text-md md:text-xl px-1 py-3 md:px-2 min-w-[35px] md:min-w-[40px]  font-bold rounded-md",
								correctLetters.includes(key)
									? "bg-green-600"
									: presentLetters.includes(key)
									? " bg-yellow-600"
									: absentLetters.includes(key)
									? "bg-neutral-700"
									: "bg-neutral-500"
							)}
							onClick={() => onKeyClick(key)}
						>
							{key === "backspace" ? "⌫" : key.toUpperCase()}
						</button>
					))}
				</div>
			))}
		</div>
	)
}
